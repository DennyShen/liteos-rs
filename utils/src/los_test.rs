/*
使用时加
#![feature(custom_test_frameworks)]
#![test_runner(utils::los_test::test_runner)]
#![reexport_test_harness_main = "test_main"]

用例
use utils::tprn;

#[test_case]
fn test1() {
	tprn!("xx test1 start");
	let a = 1;
	let b = 1;
	tprn!("a = {}, b = {}", a, b);
	assert_eq!(a, b);
	tprn!("xx test1 end");
}
*/

use cortex_m_semihosting::{hprintln};

//#[cfg(test)]
pub fn test_runner(tests: &[&dyn Fn()]) {
	hprintln!("Running {} tests", tests.len()).unwrap();
	for test in tests {
		test();
	}
}

#[macro_export]
macro_rules! tprn {
	($($arg:tt)*) => (
		cortex_m_semihosting::hprintln!($($arg)*).unwrap();
	)
}
