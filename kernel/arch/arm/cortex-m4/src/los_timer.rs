/*
 * Copyright (c) 2013-2019 Huawei Technologies Co., Ltd. All rights reserved.
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

use super::los_context::*;
use cortex_m::asm::*;

pub enum LOS_SysSleepEnum{
    OS_SYS_NORMAL_SLEEP = 0,
    OS_SYS_DEEP_SLEEP = 1,
}

//systick config
pub const SystemCoreClock:u32 = 168000000;
pub const SysTick:u32 = 0xE000E010;
pub const SysTickCTL:u32 = SysTick;
pub const SysTickLoad:u32 = SysTick + 0x4;
pub const SysTickVal:u32 = SysTick + 0x8;
pub const SysTick_CTRL_ENABLE_Msk:u32 = 1 << 0;

fn systick_config()
{
	unsafe{*(SysTickLoad as *mut u32) = SystemCoreClock / 1000 - 1};
	unsafe{*(SysTickVal as *mut u32) = 0};
	unsafe{*(SysTickCTL as *mut u32) = 1 | 1 << 1 | 1 << 2};
}

 pub fn HalTickStart() -> u32 {
	systick_config();
	0
 }

pub fn  HalSysTickReload(nextResponseTime:u64) {
    HalTickLock();
    unsafe{*(SysTickLoad as *mut u32) = (nextResponseTime - 1) as u32}; /* set reload register */
	unsafe{*(SysTickVal as *mut u32) = 0};/* Load the SysTick Counter Value */
    //NVIC_ClearPendingIRQ(SysTick_IRQn);
    HalTickUnlock();
}


pub fn  HalGetTickCycle(period: &mut u32) -> u64 {
    let mut hwCycle: u32 = 0;
    let intSave: u32 = LOS_IntLock();
    *period = unsafe{*(SysTickLoad as *mut u32)};
    hwCycle = *period - unsafe{*(SysTickVal as *mut u32)};
    LOS_IntRestore(intSave);
    return hwCycle as u64;
}

pub fn HalTickLock(){
	unsafe{*(SysTickCTL as *mut u32) &= !SysTick_CTRL_ENABLE_Msk};
}


pub fn HalTickUnlock(){
	unsafe{*(SysTickCTL as *mut u32) |= SysTick_CTRL_ENABLE_Msk};
}


pub fn HalEnterSleep(sleep :LOS_SysSleepEnum){
   dsb();
   wfi();
   isb();
}
