use utils::los_list::LosList; 
use core::mem;
use super::los_config::*;
use super::los_memory;

pub type CVoidPointer = *mut c_void;
pub type TSK_ENTRY_FUNC = fn(arg:u32)-> CVoidPointer;

pub static g_taskMaxNum: usize = LOSCFG_BASE_CORE_TSK_LIMIT + 1;

//pub static g_taskCBArray: &mut LosTaskCB = ;

#[repr(u8)]
pub enum c_void {
	__variant1,
	__variant2,
}

struct SortLinkList {
    sortLinkNode: LosList,
    responseTime: u64,
}

struct EVENT_CB_S {
	uwEventID: u32,        //< Event mask in the event control block, indicating the event that has been logically processed. 
	stEventList: LosList, //< Event control block linked list 
}

struct LosTaskCB {
	stackPointer: CVoidPointer,
	taskStatus: u16,
	priority: u16,
	timeSlice: i32,
	sortList: SortLinkList,
	startTime: u64,
	stackSize: u32,                ///**< Task stack size */
	topOfStack: u32,               ///**< Task stack top */
	taskID: u32,                   ///**< Task ID */
	taskEntry: TSK_ENTRY_FUNC,                ///**< Task entrance function */
	taskSem: CVoidPointer,                 ///**< Task-held semaphore */
	taskMux: CVoidPointer,                 ///**< Task-held mutex */
	arg: u32,                      ///**< Parameter */
	taskName: [u8;32],                ///**< Task name */
	pendList: LosList,
	timerList: LosList,
	event: EVENT_CB_S,
	eventMask: u32,                ///**< Event mask */
	eventMode: u32,               ///**< Event mode */
	msg: CVoidPointer,                    ///**< Memory allocated to queues */
	errorNo: i32,
}


pub fn OsTaskInit() -> u32 {
	let mut size:usize = 0;
	let mut index:usize = 0;

	size = (g_taskMaxNum + 1) * (mem::size_of::<LosTaskCB>());

	//g_taskCBArray = los_memory::alloc(size);


	5
}
