/*
 * Copyright (c) 2013-2019 Huawei Technologies Co., Ltd. All rights reserved.
 * Copyright (c) 2020-2021 Huawei Device Co., Ltd. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#![no_main]
#![no_std]
#![allow(dead_code)]
#![allow(non_upper_case_globals)]
#![feature(custom_test_frameworks)]
#![test_runner(utils::los_test::test_runner)]
#![reexport_test_harness_main = "test_main"]
use panic_halt as _;

use cortex_m_semihosting::{debug, hprintln};

use core::{
    mem::zeroed,
    ptr::{read, write_volatile},
};
use utils::los_list;
use kernel::los_memory;
use kernel::los_task;
use kernel::los_config::*;
use cortex_m4::los_timer;
use cortex_m4::los_context;


extern "C" {
    fn _estack();
}

pub unsafe extern "C" fn svc_handler() {
	hprintln!("svc_handler! ").unwrap();
}

pub unsafe extern "C" fn pendsv_handler() {
	hprintln!("pendsv_handler! ").unwrap();
}

pub unsafe extern "C" fn systick_handler() {
	hprintln!("systick_handler! ").unwrap();
}

pub unsafe extern "C" fn default_handler() {
	hprintln!("default_handler! ").unwrap();
}

#[link_section = ".isr_vector"]
#[used]
pub static BASE_VECTORS: [unsafe extern "C" fn(); 16] = [
    _estack,
    reset_handler,
    default_handler, // NMI
    default_handler,  // Hard Fault
    default_handler, // MemManage
    default_handler, // BusFault
    default_handler, // UsageFault
    default_handler,
    default_handler,
    default_handler,
    default_handler,
    svc_handler,         // SVC
    default_handler, // DebugMon
    default_handler,
    pendsv_handler, // PendSV
    systick_handler,     // SysTick
];

extern "C" {
	// These symbols come from `linker.ld`
	static mut _sbss: u32; // Start of .bss section
	static mut _ebss: u32; // End of .bss section
	static mut _sdata: u32; // Start of .data section
	static mut _edata: u32; // End of .data section
	static __rodata_regions_array_start: u32; // Start of .rodata section
	static mut __vectors_start:u32;
}

pub unsafe extern "C" fn reset_handler() {

	let mut sbss: *mut u32 = &mut _sbss;
	let ebss: *mut u32 = &mut _ebss;

	while sbss < ebss {
		write_volatile(sbss, zeroed());
		sbss = sbss.offset(1);
	}

	let mut sdata: *mut u32 = &mut _sdata;
	let edata: *mut u32 = &mut _edata;
	let mut sidata: *const u32 = &__rodata_regions_array_start;

	while sdata < edata {
		write_volatile(sdata, read(sidata));
		sdata = sdata.offset(1);
		sidata = sidata.offset(1);
	}

    // Call user's main function
    main()
}

#[no_mangle]
pub unsafe fn __aeabi_unwind_cpp_pr0() -> () {
  loop {}
}

fn main() -> ! {
    hprintln!("welcome, liteos rust! ").unwrap();

	hprintln!("heap init ").unwrap();
	los_memory::init(LOSCFG_SYS_HEAP_SIZE);
	hprintln!("heap size:{} init success!",LOSCFG_SYS_HEAP_SIZE).unwrap();

	hprintln!("arch init ").unwrap();
    los_context::HalArchInit();

	hprintln!("task init ").unwrap();
	los_task::OsTaskInit();

	los_memory::test1();
	los_list::test_list();

    #[cfg(test)]
    test_main();

	//systick can work;
	los_timer::HalTickStart();
	// exit QEMU
    // NOTE do not run this on hardware; it can corrupt OpenOCD state
    debug::exit(debug::EXIT_SUCCESS);

    loop {}
}

/*

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}

#[test_case]
fn testaaa() {
    assert_eq!(2, 2);
}
*/
